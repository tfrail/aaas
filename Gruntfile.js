module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
		copy: {
            'release': {
                files: [
                    {
                        src: [
                            // Copy all files in all directories
                            "./**/*",

                            // Exclude these directories
                            '!./**/{.git,_build,grunt-assets,node_modules,reference-docs}/**',

                            // Exclude these files
                            '!./{.gitignore,config.json,Gruntfile.js,npm-debug.log,package.json}'
                        ],
                        dest: "_build/release/"
                    }
                ]
            }
		},
		sass: {
            options: {
                sourceMap: false
            },
            'release': {
                files: {
                    '_build/release/ui/css/main.css': '_build/release/ui/scss/main.scss'
                }
            },
			'local': {
                files: {
                    'ui/css/main.css': 'ui/scss/main.scss'
                }
            }
        },
		autoprefixer: {
			css: {
				src: [
					"_build/release/ui/css/*.css"
				],
			},
		},
		cssmin: {
            'release': {
                files: [
                    {
                        expand: true,
                        src: [
                            // Minify all these css files
                            "_build/release/ui/**/*.css"
                        ],
                        ext: ".css",
                        extDot: 'last'
                    }
                ]
            }
        },
		uglify: {
            "release": {
                files: [
                    {
                        expand: true,
                        src: [
                            // Include these file types
                            "_build/release/ui/**/*.js"
                        ],
                        ext: ".js",
                        extDot: "last"
                    }
                ]
            }
        },
		clean: {
            'release': {
                src: ["_build/release/"]
            },
			'sass': {
                src: ["_build/release/ui/scss/"],

            }
        }
  });

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');

  // Default task(s)
  grunt.registerTask('clean.release', ['clean:release']);
  grunt.registerTask('copy.release', ['copy:release']);
  grunt.registerTask('sass.release', ['sass:release']);
	
  grunt.registerTask('build', function() {

        // Tasks
        // 1. clean _build/release/
        grunt.task.run('clean:release');

        // 2. Copy all files to be included to _build/release/
        grunt.task.run('copy:release');
		
		// 3. SASS
        grunt.task.run('sass:release');
		
		// Prefixer
		grunt.task.run('autoprefixer:css');
		
		// Remove scss file
		grunt.task.run('clean:sass');

        // 4. uglify necessary js files
        grunt.task.run('uglify:release');
		
		// 5. cssmin necessary css files
        grunt.task.run('cssmin:release');

    });
	
	grunt.registerTask('sass-local', function() {
		grunt.task.run('sass:local');
	});

};