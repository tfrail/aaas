# README #

### What is this repository for? ###

* AAAS Front End Exercise

### How do I get set up? ###

* To build, first install grunt packages -- npm install
* Run "grunt build" to perform a full build, including:
* - Creations of build/release directories
* - SCSS Compiling
* - JS/CSS minification
* - CSS auto-prefixer
* Run "grunt sass-local" to compile SCSS in the working directory. Will create a "css" directory in /ui


### Who do I talk to? ###

* Tim Frailey - tgfrailey@gmail.com
