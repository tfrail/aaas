/**
 * AAAS Front End Exercise
 * @author Tim Frailey
 */

var ScienceApp = {
	// Globals
	doiArr: [
	  '10.1111/j.1365-277x.2004.00520.x',
	  '10.1007/bf00023219',
	  '10.2514/3.59819',
	  '10.1002/3527603433.ch3',
	  '10.1017/s0034670512001088',
	  '10.1126/science.1187145',
	  '10.2478/s11696-011-0110-6',
	  '10.4028/0-87849-453-7.159',
	  '10.4028/www.scientific.net/amr.217-218.88',
	  '10.1007/s11948-014-9521-4',
	  '10.1524/ncrs.2011.0091',
	  '10.1128/jb.02242-14',
	  '10.1116/1.4904970',
	  '10.1016/j.jmbbm.2015.01.013'
	],
	uri: 'http://api.crossref.org/works?filter=doi:',
	uriKeyFilters: "&select=DOI,title,URL,author,container-title,publisher,references-count",
	articleObjs: {},
	
	// Methods	

	/**
	   * Formats URI passed in as arg and executes http request for content
	   * @param doiArr - Array or string or DOI's for http request
    */
	getContent: function(doiArr){
		// Format URI for http request
		var getUri = this.uri;
		// Check if doi is an object/array to loop and append DOI's
		if ($.isArray(doiArr)){
			for (var i = 0; i < doiArr.length; i++) {
				// If not first doi to be appended, include comma
				getUri += i > 0 ? ",doi:" + doiArr[i] : doiArr[i];
			}
		// Otherwise, just attach single DOI (scalability effort -- for future search functionality)
		} else {
			getUri += doiArr;
		}
		getUri += this.uriKeyFilters;
		var encodedUri = encodeURI(getUri);

		return $.get(encodedUri);
	},

	/**
	   * Sorts array returned from service to match order of original array of DOI's
	   * @param arrToSort - Array that needs sorting
	   * @param origSort - Original array of DOI's to use for sort matching
    */
	sortArr: function(arrToSort, origSort){
		var sortedArr = [];
		for(var i = 0; i < origSort.length; i++){
			for(var y = 0; y < arrToSort.length; y++){
				if (origSort[i] == arrToSort[y].DOI){
					sortedArr.push(arrToSort[y]);
					arrToSort.splice(y, 1);
					break;
				}	
			}
		}
		return(sortedArr);
	},

	/**
	   * Creates HTML and renders content from service on page
	   * @param contentObj - Content to render
    */
	renderSummaryContent: function(contentObj){
		var tableRows = '';
		contentObj.forEach(function(obj){
			var contentAuthor =  obj.author[0].given + ' ' + obj.author[0].family;
			// Check if more than one author - Format accordingly
			contentAuthor += obj.author.length > 1 ? ", et al." : "";

			tableRows += '<tr><td><a href="' + obj.URL +'" target="_blank">' 
				+ obj.title + '</a></td><td>' 
				+ obj['references-count'] + '</td><td>' 
				+ contentAuthor + '</td><td><em>'
				+ obj['container-title'][0] + '</em></td><td>'
				+ obj.publisher + '</td></tr>';
		});
		$(".summary-content").append(tableRows);
	},

	/**
	   * Kick off http request and initial page rendering
    */
	init: function(){
		//Get initial content from CrossRef
		this.getContent(this.doiArr).then(function(fulfilled){
			ScienceApp.articleObjs = ScienceApp.sortArr(fulfilled.message.items, ScienceApp.doiArr);
			ScienceApp.renderSummaryContent(ScienceApp.articleObjs, ScienceApp.doiArr);
			// Add sorting functionality to table
			$(".summary").tablesorter();
		}).catch(function(rejected){
			/* Error handling goes here */
			console.log("No content found");
		});
	}
}

$(document).ready(function(){
	ScienceApp.init();

	// Event Listeners
	$('.search-btn-mobile').on('click', function(){
		$('.search').slideToggle('fast');
	})

});